/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package archivos;

import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author nacho
 */
public class LeeConBuffer {
    public static void main(String[] args) {
        final String rutaArchivo="D:/Documentos/amigos.txt"; //Barras al revés o contrabarras dobles
        ArrayList<String> texto;
        File f=new File(rutaArchivo);
        try {
            texto=leeFichero(f);
            Iterator it=texto.iterator();
            while (it.hasNext()) {
                System.out.println((String)it.next());
            }
        } catch (Exception e) {
            if (f.exists()) {
                System.out.println("Error de E/S");
            } else {
                System.out.println("No se encuentra el archivo");
            }
        }
    }
    
    public static ArrayList<String> leeFichero(File f) throws IOException {
        ArrayList<String> texto=new ArrayList<>();
        FileReader fr=new FileReader(f);
        BufferedReader br=new BufferedReader(fr);
        boolean salir=false;
        String linea;
        while (!salir) {
            linea=br.readLine();
            if (linea!=null) {
                texto.add(linea);
            } else {
                salir=true;
            }
        }
        return texto;
    }
}
