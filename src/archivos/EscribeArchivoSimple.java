/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package archivos;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author nacho
 */
public class EscribeArchivoSimple {
    public static void main(String[] args) {
        final String rutaArchivo="D:/Documentos/ficheroEscritura.txt"; //Barras al revés o contrabarras dobles
        File f=new File(rutaArchivo);
        try {
            escribeFichero(f, "Lo que quiero escribir\r\nEsto va en otra línea");
        } catch (Exception e) {
            if (f.exists()) {
                System.out.println("Error de E/S");
            } else {
                System.out.println("No se encuentra el archivo");
            }
        }
    }
    
    public static void escribeFichero(File f, String contenido) throws IOException {
        FileWriter fw=new FileWriter(f);
        for (int i=0; i<contenido.length();i++) {
            fw.write(contenido.charAt(i));
        }
        // También valdría (sin el bucle):
        // fw.write(contenido);
        fw.close();
    }
    
}
