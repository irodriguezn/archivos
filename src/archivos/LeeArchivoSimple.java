/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package archivos;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author nacho
 */
public class LeeArchivoSimple {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        final String rutaArchivo="D:/Documentos/amigos.txt"; //Barras al revés o contrabarras dobles
        File f=new File(rutaArchivo);
        try {
            String contenido=leeFichero(f);
            System.out.println(contenido);
        } catch (Exception e) {
            if (f.exists()) {
                System.out.println("Error de E/S");
            } else {
                System.out.println("No se encuentra el archivo");
            }
        }
    }
    
    public static String leeFichero(File f) throws IOException {
        String contenido="";
        FileReader fr=new FileReader(f);
        int car=fr.read();
        while (car!=-1) {
            contenido+=car+" ";
            car=fr.read();
        }
        return contenido;
    }
}
