/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package archivos;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;

/**
 *
 * @author nacho
 */
public class CopiaImagen {
    public static void main(String[] args) {
        int num;
        int contador=0;
        String ruta="D:/Documentos/";
        LinkedList<Integer> contenido=new LinkedList<>();
        try {
            FileInputStream f=new FileInputStream(ruta+"aa.jpg");
            boolean eof=false;
            while (!eof) {
                num=f.read();
                if (num!=-1) {
                    contenido.add(num);
                } else {
                    eof=true;
                }
            }
            f.close();
        } catch (IOException e) {
            System.out.println("Error de Lectura");
        }
        
        try {
            FileOutputStream w=new FileOutputStream(ruta+"aa_copia.jpg");
            for (int i=0; i<contenido.size(); i++) {
                w.write(contenido.get(i));
            }
            w.close();
        } catch (IOException e) {
            System.out.println("Error escribiendo");
        }
    }

}
